#pragma once

		int Hlp_SumSquare ( int number_01 , int number_02 ) 
		{ 
			return pow ( ( number_01 + number_02 ) , 2 ); 
		}

		double Hlp_SumSquare ( double number_01 , double number_02 )
		{
			return pow ( ( number_01 + number_02 ) , 2 );
		}

		double Hlp_SumSquare ( double number_01 , int number_02 )
		{
			return pow ( ( number_01 + number_02 ) , 2 );
		}

		double Hlp_SumSquare ( int number_01 , double number_02 )
		{
			return pow ( ( number_01 + number_02 ) , 2 );
		}
